<!DOCTYPE html>
<html lang="es-BO">
  <head>
    <title>El ahorcado - Categoría: Películas</title>
	<meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/admin.css">
  </head>
  <body>
    <h1>Palabras para Categoría "Películas"</h1>
	<ul>
	  <li>Corazón valiente <a href="deleteWord.php?id=1">[X]</a></li>
	  <li>Little Boy <a href="deleteWord.php?id=1">[X]</a></li>
	  <li>Intensamente <a href="deleteWord.php?id=1">[X]</a></li>
	  <li>Star Trek <a href="deleteWord.php?id=1">[X]</a></li>
	  <li>
	  	<form method="POST" action="category.php">
		<!-- <form method="POST" action="createWord.php"> -->
		  <input type="hidden" name="categoryHidden" value="1" />
		  <input type="text" name="newWordTextBox" maxlength="50" />
		  <input type="submit" name="newWordButton" value="Crear" />
		</form>
	  </li>
	</ul>
	
	<div>
	   <a href="deleteCategory.php?categoryId=1" onclick="return confirm('¿Está seguro que desea eliminar esta categoría?');">Eliminar esta categoría</a>
	</div>
  </body>
</html>
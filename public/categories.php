<!DOCTYPE html>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "hangman"; 

$conn = mysqli_connect($servername, $username, $password, $database);

if (!$conn)
{
	die("Fallo en la conexión con la Base de Datos. " 
	   . mysqli_connect_error());
}

	$success = false;

if (isset($_POST["createNewCategoryButton"]))
{
	$categoryName = $_POST["newCategoryTextBox"];
	$sql = "INSERT INTO category(name) VALUES ('" . $categoryName . "')";
	
	if (mysqli_query($conn, $sql))
	{
		$lastId = mysqli_insert_id($conn);
		header("Location: http://localhost/hangman/public/category.php?id=" . $lastId);
		//header("Location: http://hangman.local/category.php?id=" . $lastId);
	}
}

$selectSql = "SELECT id, name FROM category";
$result = mysqli_query($conn, $selectSql);
?>
<html lang="es-BO">
  <head>
    <title>El ahorcado - Categorías</title>
	<meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/admin.css">
  </head>
  <body>
    <h1>Categorías</h1>
	<ul>
	<?php
       if (mysqli_num_rows($result) > 0) 
	   {
		   while( $row = mysqli_fetch_assoc($result) )
		   { ?>
			  <li><a href="category.php?id=<?= $row["id"] ?>"><?= $row["name"] ?></a></li>  
	 <?php
           }
	   }
	  ?>
	  <ul>
		<form method="POST" action="categories.php">
		  <input type="text" name="newCategoryTextBox" maxlength="50" />
		  <input type="submit" name="createNewCategoryButton" value="Crear" />
		</form>
	  </li>
	</ul>
  </body>
</html>
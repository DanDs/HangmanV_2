<!DOCTYPE html>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "hangman"; 

$conn = mysqli_connect($servername, $username, $password, $database);

if (!$conn)
{
	die("Fallo en la conexión con la Base de Datos. " 
	   . mysqli_connect_error());
}

	$success = false;

if (isset($_POST["createNewMovieButton"]))
{
	$movieTitle = $_POST["newMovieTextBox"];
	//$sql = "INSERT INTO movies(title) VALUES ('" . $movieTitle . "')";
	$sql = "INSERT INTO movies(title) VALUES (?)";

	$stmt = mysqli_stmt_init($conn);

	if(mysqli_stmt_prepare($stmt,$sql))
	{
			mysqli_stmt_bind_param($stmt, "s", $movieTitle); //sidb

			mysqli_stmt_execute($stmt);
	
			$lastId = mysqli_insert_id($conn);
			//header("location: http://" . $_SERVER["HTTP_HOST"] . );
			header("Location: http://localhost/hangman/public/wordregister.php?id=" . $lastId);
		//header("Location: http://hangman.local/category.php?id=" . $lastId);
			$success = true;
	}
}

$selectSql = "SELECT title FROM movies";
$result = mysqli_query($conn, $selectSql);
?>
<html lang="es-BO">
  <head>
    <title>El ahorcado - Nueva Palabra</title>
	<meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/admin.css">
  </head>
  <body>
    <h1>Peliculas</h1>
	<ul>
	<?php
       if (mysqli_num_rows($result) > 0) 
	   {
		   while( $row = mysqli_fetch_assoc($result) )
		   { ?>
			  <li><a href="wordregister.php?id=<?= $row["title"]?>"><?= $row["title"] ?></a></li>  
	 <?php
           }
	   }
	  ?>
	  <ul>
		<form method="POST" action="wordregister.php">
		  <input type="text" name="newMovieTextBox" maxlength="50" />
		  <input type="submit" name="createNewMovieButton" value="Registrar Peli" />
		</form>
	  </li>
	</ul>
  </body>
</html>
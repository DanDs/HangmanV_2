<!DOCTYPE html>
<?php
    function fetchRandomWordFromFile()
    {
	    $file = fopen("words.txt", 'r');
	    $words = array();
	   
	    if($file)
	    {
		    $line = null;
		   
		    while( ($line = fgets($file)) !== false )
		    {
			    $words = array_merge( $words, explode(",", $line) );
		    }
		   
            fclose($file);
	    }
	   
	    return $words[rand(0, count($words)-1)];
    }

	session_start();
	
	$showUserInput = false;
	$showWin = false;
	$showLoose = false;

	$letter='';
	$pos=-1;
	$revealed = '';
	
	if (!isset($_POST["letterTextBox"]) || !isset($_SESSION["revealedInfo"]))
	{
		$word=fetchRandomWordFromFile();
		
		$revealed=str_repeat("_",strlen($word));
		$health = 6;
		$showUserInput = true;
		
		
		$_SESSION["word"] = $word;
	}
	else
	{
		$word=$_SESSION["word"]; 
		 
		$revealed = $_SESSION["revealedInfo"];
		$health = $_SESSION["healthInfo"];
		
		$prevRevealed = $revealed;
		
		$letter = $_POST["letterTextBox"];
		do 
		{
			$pos=stripos($word, $letter,$pos+1);
			if($pos !== false)
			{
				$revealed[$pos] = $word[$pos];
			}
		} while($pos !== false);
		
		if ($prevRevealed === $revealed)
			$health--;
		
	    if ($health > 0)
        {
		    if ($revealed === $word) 
			    $showWin = true;
		    else
			    $showUserInput = true;
	    }
	    else
			$showLoose = true;
	}
	 
	$_SESSION["revealedInfo"] = $revealed;
	$_SESSION["healthInfo"] = $health;

?>
<html>
	<head>
	    <link rel="stylesheet" type="text/css" href="css/main.css">
	</head>
	<body>
        <?php if ($health==6) { ?>
		<img src="base.png" style="display:inline">
        <?php } ?>
        <?php if ($health==5) { ?>
		<img src="1.png" style="display:inline">
        <?php } ?>
        <?php if ($health==4) { ?>
		<img src="2.png" style="display:inline">
        <?php } ?>
        <?php if ($health==3) { ?>
		<img src="3.png" style="display:inline">
        <?php } ?>
        <?php if ($health==2) { ?>
		<img src="4.png" style="display:inline">
        <?php } ?>
        <?php if ($health==1) { ?>
		<img src="5.png" style="display:inline">
        <?php } ?>
        <?php if ($health==0) { ?>
		<img src="6.png" style="display:inline">
        <?php } ?>
	    <div>
		   <strong><?= $health ?><strong>
		</div>
		<div class="revealed">
			
			<br/>
			<?= $revealed ?>
		</div>
		
		
		<?php if ($showUserInput) { ?>
		<div class="userInput">
			<form method="POST">
			    <input type ="text" 
				       id="letterTextBox"
             		   name="letterTextBox"
					   maxlength="1" 
					   size="1"/>
				<input type ="submit" 
				       id="tryButton"
         			   name="tryButton" 
					   value="Intentar" />
			</form>		  
		</div>
		<?php } ?>

		<?php if ($showWin) { ?>
		  <div>Ganaste</div>
		<?php } ?>
		
		<?php if ($showLoose) { ?>
		<div> <h1><center><b>Perdiste</b></center></h1> </div>
		<?php } ?> 
	</body>
</html>














